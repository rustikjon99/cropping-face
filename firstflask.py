from flask import Flask, send_file, request, jsonify
import requests
import cv2



app = Flask(__name__)


@app.route('/crop-avatar', methods=['POST'])
def crop_avatar():
    response = requests.get(request.form['passport_url']) # Скачивание фото пасспорта из URL
    file = open("image.png", "wb") # Cохранение в файле image.png
    file.write(response.content)
    file.close()


    #Вырезание лица из фото

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml') #
    img = cv2.imread("image.png")

    while True:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = face_cascade.detectMultiScale(gray, 1.1, 5)

        for (x, y, w, h) in faces:
            crop_face = img[y: y + h, x: x + w]
            cv2.imwrite("upload.jpg", crop_face)

        if cv2.error:
            break

    cv2.destroyAllWindows()


    #Пост на данный урл с указанным id юзера
    url= 'https://services.test.aliftech.uz/api/gate/users/' + request.form["user_id"] + '/upload-avatar'
    headers = {'Service-Token': 'service-token-merchant'}
    files = {'image': open('upload.jpg', 'rb')}
    requests.post(url, headers=headers, files=files)


    return request.form




if __name__ == '__main__':
    app.run(host='localhost',port=8080, debug=True)